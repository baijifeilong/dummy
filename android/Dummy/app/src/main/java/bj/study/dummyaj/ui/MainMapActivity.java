package bj.study.dummyaj.ui;

import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Toast;

import org.andnav.osm.OpenStreetMapActivity;
import org.andnav.osm.views.OpenStreetMapView;

import bj.study.dummyaj.Dummy;
import bj.study.dummyaj.MyOverlay;
import bj.study.dummyaj.R;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import timber.log.Timber;

public class MainMapActivity extends OpenStreetMapActivity {

    @Bind(R.id.mapView)
    OpenStreetMapView mapView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_map);
        ButterKnife.bind(this);

        initMapView();
        initTestLayout();
    }

    private void initMapView() {
        mapView.setRenderer(getIntenetRendererInfo(getResources(), "微软中国地图"));
        mapView.setZoomLevel(8);
        mapView.setMapCenter(39d, 116d);
        mapView.setMainActivityCallbackHandler(new Handler());
        registerForContextMenu(mapView);
    }

    private void initTestLayout() {
        ViewGroup layoutTest = (ViewGroup) View.inflate(this, R.layout.layout_test, null);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.addRule(RelativeLayout.CENTER_VERTICAL);
        ((ViewGroup) mapView.getParent()).addView(layoutTest, layoutParams);

        new TestLayoutHolder(layoutTest);
    }

    public OpenStreetMapView getMapView() {
        return mapView;
    }

    @Override
    public void onLocationLost() {

    }

    @Override
    public void onLocationChanged(Location pLoc) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    class TestLayoutHolder {

        @Bind(R.id.button_test1)
        Button btnTest1;
        @Bind(R.id.button_test2)
        Button btnTest2;
        @Bind(R.id.button_test3)
        Button btnTest3;

        public TestLayoutHolder(View view) {
            ButterKnife.bind(this, view);
            init();
        }

        private void init() {
            btnTest1.setText("Init Overlay");
            btnTest2.setText("hello");
            btnTest3.setText("添加Overlay");
        }

        @OnClick(R.id.button_test1)
        void onClick1() {
            Timber.d("Init overlay");
            new MyOverlay();
        }

        @OnClick(R.id.button_test2)
        void onClick2() {
            Dummy dummy = new Dummy();
            dummy.hello();
            //dummy.helloNonAbstract();
        }

        @OnClick(R.id.button_test3)
        void addOverlays() {
            mapView.getOverlays().add(new MyOverlay());
            mapView.invalidate();
        }
    }

    @Override
    public void onBackPressed() {
        moveTaskToBack(false);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        Toast.makeText(MainMapActivity.this, "你点击里Activity", Toast.LENGTH_SHORT).show();
        Timber.d("点击了Activity");
        return super.onTouchEvent(event);
    }
}

package bj.study.dummyaj.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;

import org.andnav.osm.views.OpenStreetMapView;

import timber.log.Timber;

/**
 * @Author BaiJiFeiLong@gmail.com
 * @Date 2015/9/9 17:37
 */
public class MapView extends OpenStreetMapView {
    public MapView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        Timber.d("点击坐标(%f,%f)", event.getX(), event.getY());
        return super.onTouchEvent(event);
    }
}

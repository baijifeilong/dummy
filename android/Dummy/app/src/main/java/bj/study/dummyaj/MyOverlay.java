package bj.study.dummyaj;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.MotionEvent;
import android.widget.Toast;

import org.andnav.osm.views.OpenStreetMapView;
import org.andnav.osm.views.overlay.OpenStreetMapViewOverlay;

import timber.log.Timber;

/**
 * @Author BaiJiFeiLong@gmail.com
 * @Date 2015/9/11 19:15
 */
public class MyOverlay extends OpenStreetMapViewOverlay {
    @Override
    protected void onDraw(Canvas c, OpenStreetMapView osmv) {
        Timber.d("onDraw被呼叫");
        Paint paint = new Paint();
        paint.setColor(0x33009933);
        paint.setStrokeWidth(0);
        c.drawRect(10, 10, 710, 1200, paint);
    }

    @Override
    protected void onDrawFinished(Canvas c, OpenStreetMapView osmv) {
    }

    @Override
    public boolean onTouchEvent(MotionEvent event, OpenStreetMapView mapView) {
        Toast.makeText(App.getContext(), "你点击里覆盖层", Toast.LENGTH_SHORT).show();
        Timber.d("点击里覆盖层");
        return true;
    }
}

package bj.study.dummyaj.ui;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import bj.study.dummyaj.R;

/**
 * @Author BaiJiFeiLong@gmail.com
 * @Date 2015/9/14 16:40
 */
public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportFragmentManager().beginTransaction().add(R.id.fragment_container_1, new FirstFragment())
                .add(R.id.fragment_container_2, new SecondFragment()).commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu1, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container_1, new SecondFragment())
                .replace(R.id.fragment_container_2, new FirstFragment())
                .addToBackStack(null).commit();
        String str = String.format("内部存储:%s, 缓存:%s, 外部存储:%s, 外部缓存:%s", getFilesDir(), getCacheDir(),
                getExternalFilesDir(null), getExternalCacheDir());
        Toast.makeText(this, str, Toast.LENGTH_LONG).show();
        return super.onOptionsItemSelected(item);
    }
}

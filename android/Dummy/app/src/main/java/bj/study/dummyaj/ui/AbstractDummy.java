package bj.study.dummyaj.ui;

import timber.log.Timber;

/**
 * @Author BaiJiFeiLong@gmail.com
 * @Date 2015/9/11 20:05
 */
public abstract class AbstractDummy {
    public void hello() {
        Timber.d("P hello");
    }

    public void helloNonAbstract() {
        Timber.d("Non Abstract Hello");
    }
}

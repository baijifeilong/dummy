package bj.study.dummyaj;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.SimpleAdapter;

import java.util.List;
import java.util.Map;

/**
 * @Author BaiJiFeiLong@gmail.com
 * @Date 2015/8/20 16:05
 */
public class SettingsAdapter extends ArrayAdapter<String> {

    public SettingsAdapter(Context context, int resource, String[] objects) {
        super(context, resource, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        if (position == 0) {
            view = View.inflate(getContext(), R.layout.layout_settings_user, null);
        }else {
            view = View.inflate(getContext(), R.layout.settings_item, null);
        }
        return view;
    }
}

package bj.study.dummyaj;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @Author BaiJiFeiLong@gmail.com
 * @Date 2015/8/20 15:14 15:15
 */
public class TitleLayout extends LinearLayout {

    @Bind(R.id.button_back)
    Button btnBack;

    public TitleLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        //LayoutInflater.from(context).inflate(R.layout.layout_title, this);
        View.inflate(context, R.layout.layout_title, this);
        ButterKnife.bind(this);

    }

    @OnClick(R.id.button_back)
    void update() {
        ((Activity)getContext()).finish();
    }
}

package bj.study.dummyaj.ui;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.AnalogClock;

/**
 * @Author BaiJiFeiLong@gmail.com
 * @Date 2015/9/14 17:28
 */
public class SecondActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(new AnalogClock(this));

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }


}

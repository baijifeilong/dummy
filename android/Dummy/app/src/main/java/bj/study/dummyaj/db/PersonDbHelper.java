package bj.study.dummyaj.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by BaiJiFeiLong on 2015/9/17 19:09.
 */
public class PersonDbHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 2;
    private static final String DATABASE_NAME = "person.sqlite";

    private static final String SQL_CREATE_PERSONS = "CREATE TABLE person (age number, name string)";
    private static final String SQL_CREATE_PERSONS2 = "CREATE TABLE person (age number, name text)";
    private static final String SQL_DELETE_PERSONS = "DROP TABLE IF EXISTS person";

    public PersonDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_PERSONS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_PERSONS);
        db.execSQL(SQL_CREATE_PERSONS2);
    }
}

package bj.study.dummyaj.ui;

import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.common.io.Files;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;

import bj.study.dummyaj.R;
import bj.study.dummyaj.db.PersonDbHelper;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by BaiJiFeiLong on 2015/9/15 17:22.
 */
public class SecondFragment extends Fragment {

    @Bind(R.id.button1)
    Button btn1;
    @Bind(R.id.button2)
    Button btn2;
    @Bind(R.id.textView1)
    TextView txt1;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_second, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();

        btn1.setText("创建数据库");
        btn2.setText("读取文件");
    }

    @OnClick(R.id.button1)
    void a() {
        PersonDbHelper personDbHelper = new PersonDbHelper(getActivity());
        SQLiteDatabase db = personDbHelper.getWritableDatabase();
    }

    @OnClick(R.id.button2)
    void b() {
        String s = "去不成c";
        txt1.setText(String.format("%s的长度%d", s, s.length()));
    }
}

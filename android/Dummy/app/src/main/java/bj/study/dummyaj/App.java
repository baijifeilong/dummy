package bj.study.dummyaj;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.taobao.android.dexposed.DexposedBridge;
import com.taobao.android.dexposed.XC_MethodHook;
import com.taobao.android.dexposed.XC_MethodReplacement;

import org.andnav.osm.views.overlay.OpenStreetMapViewOverlay;

import bj.study.dummyaj.ui.AbstractDummy;
import timber.log.Timber;

/**
 * @Author BaiJiFeiLong@gmail.com
 * @Date 2015/9/9 17:23
 */
public class App extends Application {

    private static Toast sToast;
    public static Context sContext;
    // 位置管理器
    public static LocationManager sLocationManager;

    @Override
    public void onCreate() {
        super.onCreate();
        sContext = getApplicationContext();
        sLocationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        initLogger();
        di();
    }

    private void initLogger() {
        // 初始化 Timber 日志工具
        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree() {

                @Override
                protected void log(int priority, String tag, String message, Throwable t) {
                    tag = "corpro_" + tag;
                    super.log(priority, tag, message, t);
                }

            });
        } else {
            Timber.plant(new Timber.Tree() {

                @Override
                protected void log(int priority, String tag, String message, Throwable t) {
                    if (priority >= Log.ERROR) {
                        tag = "corpro_" + tag;
                        Log.println(priority, tag, message);
                    }
                }
            });
        }
    }

    private void di() {
        if (DexposedBridge.canDexposed(this)) {
            Timber.d("[DI] can dexposed");

            XC_MethodHook constructorHook = new XC_MethodHook() {
                @Override
                protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                    Timber.d("[DI-Construct] %s +++", param.thisObject.getClass().getName());
                }
            };

            XC_MethodHook callHook = new XC_MethodHook() {
                @Override
                protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                    Timber.d("[DI-Call] %s", param.thisObject.getClass().getName());
                }
            };

            DexposedBridge.hookAllConstructors(Activity.class, constructorHook);
            // DexposedBridge.hookAllConstructors(ViewGroup.class, constructorHook);
            DexposedBridge.hookAllConstructors(OpenStreetMapViewOverlay.class, constructorHook);

            DexposedBridge.findAndHookMethod(Activity.class, "onDestroy", new XC_MethodHook() {
                @Override
                protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
                    Timber.d("[DI] %s ---", param.thisObject.getClass().getName());
                }
            });

            DexposedBridge.hookAllConstructors(AbstractDummy.class, constructorHook);
            DexposedBridge.findAndHookMethod(AbstractDummy.class, "hello", callHook);
            DexposedBridge.findAndHookMethod(AbstractDummy.class, "helloNonAbstract", callHook);

            DexposedBridge.hookAllMethods(MyOverlay.class, "onDraw", callHook);

        } else {
            Timber.d("[DI] can not dexposed");
        }
    }

    /**
     * 获取App上下文
     *
     * @return
     */
    public static Context getContext() {
        return sContext;
    }

    /**
     * 获取位置管理器
     *
     * @return
     */
    public static LocationManager getLocationManager() {
        return sLocationManager;
    }

    /**
     * 显示Toast
     *
     * @param message
     */
    public static void showToast(String message) {
        if (sToast != null) {
            sToast.cancel();
            sToast = null;
        }
        sToast = Toast.makeText(sContext, message, Toast.LENGTH_SHORT);
        sToast.show();
    }

}

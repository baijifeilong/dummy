package bj.study.dummyaj;

import bj.study.dummyaj.ui.AbstractDummy;
import timber.log.Timber;

/**
 * @Author BaiJiFeiLong@gmail.com
 * @Date 2015/9/11 18:47
 */
public class Dummy extends AbstractDummy{

    public Dummy() {
    }

    @Override
    public void hello() {
        Timber.d("hello");
    }
}

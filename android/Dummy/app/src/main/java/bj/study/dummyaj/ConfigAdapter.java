package bj.study.dummyaj;

import android.content.Context;
import android.database.DataSetObserver;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import java.util.List;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by BaiJiFeiLong on 2015/8/20 21:48.
 */
public class ConfigAdapter extends BaseAdapter {

    private Context mContext;
    private String[] data;

    public ConfigAdapter(Context context, String[] data) {
        this.data = data;
        mContext = context;
    }

    @Override
    public int getCount() {
        return data.length;
    }

    @Override
    public Object getItem(int position) {
        return data[position];
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView != null) {
            holder = (ViewHolder) convertView.getTag();
        } else {
            convertView = View.inflate(mContext, R.layout.settings_item, null);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        }

        holder.button1.setText((String) getItem(position));

        return convertView;
    }

    class ViewHolder {
        @Bind(R.id.button1)
        Button button1;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}

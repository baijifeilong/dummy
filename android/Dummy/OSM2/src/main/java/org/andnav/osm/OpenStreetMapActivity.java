// Created by plusminus on 00:14:42 - 02.10.2008
package org.andnav.osm;
import java.io.File;

import org.andnav.osm.util.constants.OpenStreetMapConstants;
import org.andnav.osm.views.OpenStreetMapView;
import org.andnav.osm.views.util.OpenStreetMapRendererInfo;
import org.andnav.osm.views.util.OpenStreetMapTileFilesystemProvider;

import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.GeofenceClient;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.baidu.location.LocationClientOption.LocationMode;

import android.app.Activity;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Baseclass for Activities who want to contribute to the OpenStreetMap Project.
 * 
 * @author Nicolas Gramlich
 * 
 */
public abstract class OpenStreetMapActivity extends Activity implements
		OpenStreetMapConstants {
	// ===========================================================
	// Constants
	// ===========================================================

	protected static final String PROVIDER_NAME = LocationManager.GPS_PROVIDER;

	// ===========================================================
	// Fields
	// ===========================================================

	protected SampleLocationListener mLocationListener, mNetListener;

	// protected RouteRecorder mRouteRecorder = new RouteRecorder();

	protected boolean mDoGPSRecordingAndContributing;

	protected LocationManager mLocationManager;

	public int mNumSatellites = NOT_SET;

	private boolean mGPSFastUpdate = false;
	public LocationClient mLocationClient;
	public GeofenceClient mGeofenceClient;
	public MyLocationListener mMyLocationListener;
	public static Location mycurrentLocation1 = null;
	public TextView trigger, exit;
	public Vibrator mVibrator;
	
	//sd卡上工程文件的文件夹名称
	public static String mProjectName = "gtmaps";

	// ===========================================================
	// Constructors
	// ===========================================================

	/**
	 * Calls
	 * <code>onCreate(final Bundle savedInstanceState, final boolean pDoGPSRecordingAndContributing)</code>
	 * with <code>pDoGPSRecordingAndContributing == true</code>.<br/>
	 * That means it automatically contributes to the OpenStreetMap Project in
	 * the background.
	 * 
	 * @param savedInstanceState
	 */
	
	
	public void onCreate(final Bundle savedInstanceState) {
		//initBaiduLoc();
		onCreate(savedInstanceState, true);
	}

	
	/**
	 * Called when the activity is first created. Registers LocationListener.
	 *
	 * @param savedInstanceState
	 * @param pDoGPSRecordingAndContributing
	 *            If <code>true</code>, it automatically contributes to the
	 *            OpenStreetMap Project in the background.
	 */
	public void onCreate(final Bundle savedInstanceState,
			final boolean pDoGPSRecordingAndContributing) {
		super.onCreate(savedInstanceState);
		initBaiduLoc();
		// if(pDoGPSRecordingAndContributing)
		// this.enableDoGPSRecordingAndContributing();
		// else
		// this.disableDoGPSRecordingAndContributing(false);

		SharedPreferences pref = PreferenceManager
				.getDefaultSharedPreferences(this);
		mGPSFastUpdate = pref.getBoolean("pref_gpsfastupdate", true);

		// register location listener
		initLocation();
	}

	private void initBaiduLoc() {	
		Log.i("BaiduLocationApiDem", "initBaiduLoc");
		mycurrentLocation1 = new Location("");
		mLocationClient = new LocationClient(this.getApplicationContext());
		mMyLocationListener = new MyLocationListener();
		mLocationClient.registerLocationListener(mMyLocationListener);
		mGeofenceClient = new GeofenceClient(getApplicationContext());
		mVibrator = (Vibrator) getApplicationContext().getSystemService(
				Service.VIBRATOR_SERVICE);
		InitLocation();
		mLocationClient.start();

	}
	private void InitLocation(){
		LocationClientOption option = new LocationClientOption();
		option.setLocationMode( LocationMode.Hight_Accuracy);
		option.setCoorType("gcj02");
		int span=1000;
		try {
			//span = Integer.valueOf(frequence.getText().toString());
		} catch (Exception e) {
			// TODO: handle exception
		}
		option.setScanSpan(span);
		option.setIsNeedAddress(false);
		mLocationClient.setLocOption(option);
	}
	private LocationManager getLocationManager() {
		if (this.mLocationManager == null)
			this.mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		return this.mLocationManager;
	}

	private void getBestProvider() {
		int minTime = 0;
		int minDistance = 0;

		if (!mGPSFastUpdate) {
			minTime = 2000;
			minDistance = 5;
		}
		;

		getLocationManager().removeUpdates(mLocationListener);
		if (mNetListener != null)
			getLocationManager().removeUpdates(mNetListener);

		if (getLocationManager().isProviderEnabled(GPS)) {
			getLocationManager().requestLocationUpdates(GPS, minTime,
					minDistance, this.mLocationListener);

			try {
				if (getLocationManager().isProviderEnabled(NETWORK)) {
					this.mNetListener = new SampleLocationListener();
					getLocationManager().requestLocationUpdates(NETWORK,
							minTime, minDistance, this.mNetListener);
				}
			} catch (Exception e) {
				Log.e(DEBUGTAG, "isProviderEnabled(NETWORK) exception");
				e.printStackTrace();
			}

		} else if (getLocationManager().isProviderEnabled(NETWORK)) {
			getLocationManager().requestLocationUpdates(NETWORK, minTime,
					minDistance, this.mLocationListener);
		}
	}

	protected void initLocation() {
		this.mLocationListener = new SampleLocationListener();
	}

	// ===========================================================
	// Getter & Setter
	// ===========================================================

	// ===========================================================
	// Methods from SuperClass/Interfaces
	// ===========================================================

	public abstract void onLocationLost();

	public abstract void onLocationChanged(final Location pLoc);

	public abstract void onStatusChanged(String provider, int status,
			Bundle extras);

	/**
	 * Called when activity is destroyed. Unregisters LocationListener.
	 */
	@Override
	protected void onDestroy() {
		super.onDestroy();
		getLocationManager().removeUpdates(mLocationListener);
		if (mNetListener != null)
			getLocationManager().removeUpdates(mNetListener);

		// if(this.mDoGPSRecordingAndContributing){
		// OSMUploader.uploadAsync(this.mRouteRecorder.getRecordedGeoPoints());
		// }
		//mLocationClient.stop();
	}

	// ===========================================================
	// Methods
	// ===========================================================

	// public void enableDoGPSRecordingAndContributing(){
	// /* If already true, return. */
	// if(this.mDoGPSRecordingAndContributing)
	// return;
	//
	// this.mRouteRecorder = new RouteRecorder();
	//
	// this.mDoGPSRecordingAndContributing = true;
	// }
	//
	// public void disableDoGPSRecordingAndContributing(final boolean
	// pContributdeCurrentRoute){
	// /* If already false, return. */
	// if(!this.mDoGPSRecordingAndContributing)
	// return;
	//
	// if(pContributdeCurrentRoute){
	// OSMUploader.uploadAsync(this.mRouteRecorder.getRecordedGeoPoints());
	// }
	//
	// this.mRouteRecorder = null;
	//
	// this.mDoGPSRecordingAndContributing = false;
	// }

	@Override
	protected void onStart() {
		getBestProvider();
		super.onStart();
	}

	@Override
	protected void onStop() {
		getLocationManager().removeUpdates(mLocationListener);
		super.onStop();
	}

	protected void StatusChanged(String provider, int status, Bundle b) {
		// Log.e(DEBUGTAG, "onStatusChanged povider = " + provider +
		// " status = " + status + " satellites = " + b.getInt("satellites",
		// NOT_SET));
		if (mNetListener != null) {
			if (provider.equals(GPS) && status == 2) {
				getLocationManager().removeUpdates(mNetListener);
				mNetListener = null;
				Log.e(DEBUGTAG, "Stop NETWORK listener");
			}
		}
		if (mNetListener == null)
			OpenStreetMapActivity.this.onStatusChanged(provider, status, b);
		else if (mNetListener != null && provider.equals(NETWORK))
			OpenStreetMapActivity.this.onStatusChanged(provider, status, b);

	}

	// ===========================================================
	// Inner and Anonymous Classes
	// ===========================================================

	/**
	 * Logs all Location-changes to <code>mRouteRecorder</code>.
	 * 
	 * @author plusminus
	 */
	private class SampleLocationListener implements LocationListener {
		public void onLocationChanged(final Location loc) {
			if (loc != null) {
				// if(OpenStreetMapActivity.this.mDoGPSRecordingAndContributing)
				// OpenStreetMapActivity.this.mRouteRecorder.add(loc,
				// OpenStreetMapActivity.this.mNumSatellites);

				OpenStreetMapActivity.this.onLocationChanged(loc);

			} else {
				OpenStreetMapActivity.this.onLocationLost();
			}
		}

		public void onStatusChanged(String a, int status, Bundle b) {
			OpenStreetMapActivity.this.mNumSatellites = b.getInt("satellites",
					NOT_SET); // LATER Check on an actual device
			// Log.e(DEBUGTAG, "onStatusChanged status = " + status +
			// " satellites = " + b.getInt("satellites", NOT_SET));
			StatusChanged(a, status, b);
		}

		public void onProviderEnabled(String a) {
			Log.i("abc", "onProviderEnable");
			getBestProvider();
		}

		public void onProviderDisabled(String a) {
			Log.i("abc", "onProviderDisabled");

			getBestProvider();
		}
	}
    /**
     * 百度定位监听
     * @author zhenxingcui
     *
     */
	public class MyLocationListener implements BDLocationListener {

		@Override
		public void onReceiveLocation(BDLocation location) {
			// Receive Location
			Log.i("BaiduLocationApiDem", "onReceiveLocation");
			mycurrentLocation1.setLongitude(location.getLongitude());
			mycurrentLocation1.setLatitude(location.getLatitude());
			OpenStreetMapActivity.this.onLocationChanged(mycurrentLocation1);
			StringBuffer sb = new StringBuffer(256);
			sb.append("time : ");
			sb.append(location.getTime());
			sb.append("\nerror code : ");
			sb.append(location.getLocType());
			sb.append("\nlatitude : ");
			sb.append(location.getLatitude());
			sb.append("\nlontitude : ");
			sb.append(location.getLongitude());
			sb.append("\nradius : ");
			sb.append(location.getRadius());
			if (location.getLocType() == BDLocation.TypeGpsLocation) {
				sb.append("\nspeed : ");
				sb.append(location.getSpeed());
				sb.append("\nsatellite : ");
				sb.append(location.getSatelliteNumber());
				sb.append("\ndirection : ");
				sb.append("\naddr : ");
				sb.append(location.getAddrStr());
				sb.append(location.getDirection());
			} else if (location.getLocType() == BDLocation.TypeNetWorkLocation) {
				sb.append("\naddr : ");
				sb.append(location.getAddrStr());
				// ��Ӫ����Ϣ
				sb.append("\noperationers : ");
				sb.append(location.getOperators());
			}
			Log.i("BaiduLocationApiDem", sb.toString());
			
		}

	}
	
	public OpenStreetMapRendererInfo getIntenetRendererInfo(
			final Resources aRes, final String aName) {
		OpenStreetMapRendererInfo RendererInfo = new OpenStreetMapRendererInfo(
				aRes, aName);
		RendererInfo.LoadFromResources(aName,
				PreferenceManager.getDefaultSharedPreferences(this));

		setTitle("now map:" + aName);

		return RendererInfo;
	}

	// 设置本地切片数据源（格式为sqlite数据库，后缀为）

	public OpenStreetMapRendererInfo getLocalDatabase(final Resources aRes,
			String dbName,String dbpath) {
		// 拼接标记，以便被底层识别
		if (!new File(dbpath).exists()) {
			return null;
		}
		String localDbUrl = "usermap_" + dbName + "_sqlitedb";
		//存储在share中
		SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(this).edit();
		editor.putString("MapName", localDbUrl);
		editor.putString("pref_usermaps_" + dbName+ "_sqlitedb_baseurl",dbpath);
		editor.commit();
		OpenStreetMapRendererInfo RendererInfo = new OpenStreetMapRendererInfo(
				aRes, localDbUrl);
		RendererInfo.LoadFromResources(localDbUrl,
				PreferenceManager.getDefaultSharedPreferences(this));
		return RendererInfo;
	}
	
	
	
}

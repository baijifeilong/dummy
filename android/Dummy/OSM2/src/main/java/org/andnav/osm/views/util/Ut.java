package org.andnav.osm.views.util;


import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import org.andnav.osm.OpenStreetMapActivity;
import org.andnav.osm.util.GeoPoint;
import org.andnav.osm.util.constants.OpenStreetMapConstants;
import org.andnav.osm.views.util.constants.OpenStreetMapViewConstants;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.geom.MultiLineString;
import com.vividsolutions.jts.geom.MultiPoint;
import com.vividsolutions.jts.geom.MultiPolygon;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.geom.Polygon;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.preference.PreferenceManager;
import android.util.Log;

public class Ut implements OpenStreetMapConstants, OpenStreetMapViewConstants {

	final static String[] formats = new String[] {
			"yyyy-MM-dd'T'HH:mm:ss.SSSZ", "yyyy-MM-dd'T'HH:mm:ssZ",
			"yyyy-MM-dd'T'HH:mmZ", "yyyy-MM-dd'T'HH:mm:ss'Z'",
			"yyyy-MM-dd HH:mm:ss.SSSZ", "yyyy-MM-dd HH:mmZ",
			"yyyy-MM-dd HH:mm", "yyyy-MM-dd", };

	public static Date ParseDate(final String str) {
		SimpleDateFormat sdf = new SimpleDateFormat();
		sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
		Date date = new Date(0);
		for (String format : formats) {
			sdf.applyPattern(format);

			try {
				date = sdf.parse(str);
				break;
			} catch (ParseException e) {
			}
		}

		return date;
	}

	public static boolean equalsIgnoreCase(String string, int start, int end,
			String string2) {
		try {
			return string.substring(start, end).equalsIgnoreCase(string2);
		} catch (Exception e) {
			return false;
		}
	}

	
	public static String getAppVersion(Context ctx) {
		PackageInfo pi;
		String res = "";
		try {
			pi = ctx.getPackageManager().getPackageInfo("gt.software.maps", 0);
			res = pi.versionName;
		} catch (NameNotFoundException e) {
		}

		return res;
	}

	public static void dd(String str) {
		Log.d(DEBUGTAG, str);
	}

	public static void e(String str) {
		if (DEBUGMODE)
			Log.e(DEBUGTAG, str);
	}

	public static void i(String str) {
		if (DEBUGMODE)
			Log.i(DEBUGTAG, str);
	}

	public static void w(String str) {
		if (DEBUGMODE)
			Log.w(DEBUGTAG, str);
	}

	public static void d(String str) {
		if (DEBUGMODE)
			Log.d(DEBUGTAG, str);
	}

	public static String FileName2ID(String name) {
		return name.replace(".", "_").replace(" ", "_").replace("-", "_")
				.trim();
	}

	private static File getDir(final Context mCtx, final String aPref,
			final String aDefaultDirName, final String aFolderName) {
		final SharedPreferences pref = PreferenceManager
				.getDefaultSharedPreferences(mCtx);
		Log.i("AAA", "aDefaultDirName=" + aDefaultDirName);
		final String dirName = pref.getString(aPref, aDefaultDirName) + "/"
				+ aFolderName + "/";
		Log.i("AAA", "dirName=" + dirName);
		final File dir = new File(dirName.replace("//", "/").replace("//", "/"));
		if (!dir.exists()) {
			if (android.os.Environment.getExternalStorageState().equals(
					android.os.Environment.MEDIA_MOUNTED)) {
				dir.mkdirs();
			}
		}
		Log.i("AAA", "utdir=" + dir.getAbsolutePath() + ".." + dirName);
		return dir;
	}

	public static File getRMapsMainDir(final Context mCtx,
			final String aFolderName) {
		return getDir(mCtx, "pref_dir_main", "/sdcard/" + OpenStreetMapActivity.mProjectName + "/", aFolderName);
	}

	public static File getRMapsMapsDir(final Context mCtx) {
		return getDir(mCtx, "pref_dir_maps", "/sdcard/" + OpenStreetMapActivity.mProjectName + "/maps/", "");
	}

	public static File getRMapsImportDir(final Context mCtx) {
		return getDir(mCtx, "pref_dir_import", "/sdcard/" + OpenStreetMapActivity.mProjectName + "/import/", "");
	}

	public static File getRMapsExportDir(final Context mCtx) {
		return getDir(mCtx, "pref_dir_export", "/sdcard/" + OpenStreetMapActivity.mProjectName + "/export/", "");
	}

	public static String readString(final InputStream in, final int size)
			throws IOException {
		byte b[] = new byte[size];

		int lenght = in.read(b);
		if (b[0] == 0)
			return "";
		else if (lenght > 0)
			return new String(b, 0, lenght);
		else
			return "";
	}

	public static String formatGeoPoint(GeoPoint point) {
		return point.toDoubleString();
	}

	public static CharSequence formatGeoCoord(double double1) {
		return new StringBuilder().append(double1).toString();
	}

	public static int readInt(final InputStream in) throws IOException {
		int res = 0;
		byte b[] = new byte[4];

		if (in.read(b) > 0)
			res = (((int) (b[0] & 0xFF)) << 24) + +((b[1] & 0xFF) << 16)
					+ +((b[2] & 0xFF) << 8) + +(b[3] & 0xFF);

		return res;
	}

	public static class TextWriter {
		private String mText;
		private int mMaxWidth;
		private int mMaxHeight;
		private int mTextSize;
		private Paint mPaint;
		private String[] mLines;

		public TextWriter(int aMaxWidth, int aTextSize, String aText) {
			mMaxWidth = aMaxWidth;
			mTextSize = aTextSize;
			mText = aText;
			mPaint = new Paint();
			mPaint.setAntiAlias(true);
			// mPaint.setTypeface(Typeface.create((Typeface)null,
			// Typeface.BOLD));

			final float[] widths = new float[mText.length()];
			this.mPaint.setTextSize(mTextSize);
			this.mPaint.getTextWidths(mText, widths);

			final StringBuilder sb = new StringBuilder();
			int maxWidth = 0;
			int curLineWidth = 0;
			int lastStop = 0;
			int i;
			int lastwhitespace = 0;
			/*
			 * Loop through the charwidth array and harshly insert a linebreak,
			 * when the width gets bigger than DESCRIPTION_MAXWIDTH.
			 */
			for (i = 0; i < widths.length; i++) {
				if (!Character.isLetter(mText.charAt(i))
						&& mText.charAt(i) != ',')
					lastwhitespace = i;

				float charwidth = widths[i];

				if (curLineWidth + charwidth > mMaxWidth) {
					if (lastStop == lastwhitespace)
						i--;
					else
						i = lastwhitespace;

					sb.append(mText.subSequence(lastStop, i));
					sb.append('\n');

					lastStop = i;
					maxWidth = Math.max(maxWidth, curLineWidth);
					curLineWidth = 0;
				}

				curLineWidth += charwidth;
			}
			/* Add the last line to the rest to the buffer. */
			if (i != lastStop) {
				final String rest = mText.substring(lastStop, i);

				maxWidth = Math.max(maxWidth,
						(int) this.mPaint.measureText(rest));

				sb.append(rest);
			}
			mLines = sb.toString().split("\n");

			mMaxWidth = maxWidth;
			mMaxHeight = mLines.length * mTextSize;
		}

		public void Draw(final Canvas c, final int x, final int y) {
			for (int j = 0; j < mLines.length; j++) {
				c.drawText(mLines[j].trim(), x, y + mTextSize * (j + 1), mPaint);
			}
		}

		public int getWidth() {
			return mMaxWidth;
		}

		public int getHeight() {
			return mMaxHeight;
		}
	}

	public static Intent SendMail(String subject, String text) {
		final String[] email = { "robertk506@gmail.com" };
		Intent sendIntent = new Intent(Intent.ACTION_SEND);
		sendIntent.putExtra(Intent.EXTRA_TEXT, text);
		sendIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
		sendIntent.putExtra(Intent.EXTRA_EMAIL, email);
		sendIntent.setType("message/rfc822");
		return Intent.createChooser(sendIntent, "Error report to the author");
	}

	public static int dip2px(Context context, float dipValue) {
		final float scale = context.getResources().getDisplayMetrics().density;
		return (int) (dipValue * scale + 0.5f);
	}

	public static int px2dip(Context context, float pxValue) {
		final float scale = context.getResources().getDisplayMetrics().density;
		return (int) (pxValue / scale + 0.5f);
	}

	public static void saveChooseState(Context context, String type,
			String tableName, String id) {
		SharedPreferences settings = context.getSharedPreferences(
				"choosestate", 0);
		SharedPreferences.Editor localEditor = settings.edit();
		localEditor.putString("type", type);
		if (tableName != null)
			localEditor.putString("tableName", tableName);
		if (id != null)
			localEditor.putString("id", id);
		localEditor.commit();
	}

	public static void clearChoose(Context context) {
		SharedPreferences settings = context.getSharedPreferences(
				"choosestate", 0);
		SharedPreferences.Editor localEditor = settings.edit();
		localEditor.putString("type", "");
		localEditor.putString("tableName", "");
		localEditor.putString("id", "");
		localEditor.commit();
	}

	public static Map<String, Object> getChooseState(Context context) {
		SharedPreferences settings = context.getSharedPreferences(
				"choosestate", 0);
		String id = settings.getString("id", "");
		String tableName = settings.getString("tableName", "");
		String type = settings.getString("type", "");
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("id", id);
		result.put("tableName", tableName);
		result.put("type", type);

		return result;
	}

	public static List<Coordinate[]> getListCoorsFromSqlite(Geometry geom) {
		// 从数据库中读

		// 判断geometry类型
		if (geom instanceof Polygon) {
			return getListCoorsFromPolygon((Polygon) geom);
		} else if (geom instanceof MultiPolygon) {
			return getListCoorsFromMultiPolygon((MultiPolygon) geom);
		}

		if (geom instanceof Point) {
			Point p = (Point) geom;
			List<Coordinate[]> lcoors = new ArrayList<Coordinate[]>();
			Coordinate coors[] = new Coordinate[1];
			coors[0] = p.getCoordinate();
			lcoors.add(coors);

			return lcoors;
		} else if (geom instanceof MultiPoint) {
			MultiPoint multip = (MultiPoint) geom;

			List<Coordinate[]> lcoors = new ArrayList<Coordinate[]>();

			int numGeometries = multip.getNumGeometries();
			Coordinate[] coors = new Coordinate[numGeometries];
			for (int i = 0; i < numGeometries; i++) {
				Point p = (Point) multip.getGeometryN(i);
				Coordinate c = new Coordinate(p.getX(), p.getY());
				coors[i] = c;
			}
			lcoors.add(coors);

			return lcoors;
		}

		if (geom instanceof LineString) {
			LineString lineString = (LineString) geom;
			List<Coordinate[]> lcoors = new ArrayList<Coordinate[]>();

			int n = lineString.getNumPoints();
			Coordinate[] coors = new Coordinate[n];
			// int count = 0;
			for (int i = 0; i < n; i++) {
				coors[i] = lineString.getCoordinateN(i);
			}
			lcoors.add(coors);

			return lcoors;
		} else if (geom instanceof MultiLineString) {
			MultiLineString mls = (MultiLineString) geom;
			List<Coordinate[]> lcoors = new ArrayList<Coordinate[]>();
			for (int i = 0; i < mls.getNumGeometries(); i++) {
				LineString lineString = (LineString) mls.getGeometryN(i);
				int n = lineString.getNumPoints();
				Coordinate[] coors = new Coordinate[n];
				// int count = 0;
				for (int j = 0; j <= n; j++) {
					coors[j] = lineString.getCoordinateN(j);
				}
				lcoors.add(coors);

			}
			return lcoors;
		}

		return null;
	}

	public static List<Coordinate[]> getListCoorsFromPolygon(Polygon geom) {
		// TODO Auto-generated method stub

		List<Coordinate[]> lcoors = new ArrayList<Coordinate[]>();

		Polygon p = (Polygon) geom;
		LineString out = p.getExteriorRing();
		Coordinate[] csout = out.getCoordinates();

		lcoors.add(csout);
		int innercount = p.getNumInteriorRing();
		for (int f = 0; f < innercount; f++) {
			LineString l = p.getInteriorRingN(f);
			Coordinate[] cs = l.getCoordinates();
			lcoors.add(cs);
		}

		return lcoors;

	}

	public static List<Coordinate[]> getListCoorsFromMultiPolygon(
			MultiPolygon geom) {
		// TODO Auto-generated method stub

		List<Coordinate[]> lcoors = new ArrayList<Coordinate[]>();

		for (int i = 0; i < geom.getNumGeometries(); i++) {
			Polygon polygon = (Polygon) geom.getGeometryN(i);
			List<Coordinate[]> polygonCoors = getListCoorsFromPolygon(polygon);
			for (int j = 0; j < polygonCoors.size(); j++) {
				lcoors.add(polygonCoors.get(j));
			}
		}
		return lcoors;

	}

}

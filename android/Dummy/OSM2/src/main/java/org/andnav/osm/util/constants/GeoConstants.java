// Created by plusminus on 17:41:55 - 16.10.2008
package org.andnav.osm.util.constants;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.andnav.osm.util.GeoPoint;

import android.graphics.Bitmap;



public interface GeoConstants {
	// ===========================================================
	// Final Fields
	// ===========================================================
	
	public static final int RADIUS_EARTH_METERS = 6378140;
	
	// ===========================================================
	// Methods
	// ===========================================================
	public static final Map<Bitmap,GeoPoint> armyPoint = new HashMap<Bitmap,GeoPoint>();//鍥惧儚瀵瑰簲鐨勭偣
	public static final Map<String,Bitmap> armyNameB = new HashMap<String,Bitmap>();//鍚嶇О瀵瑰簲鐨勫浘鍍�
	public static final List<String> listname = new ArrayList<String>();//鍥惧儚鍚嶇О
	public static final List<Bitmap> listbitmap = new ArrayList<Bitmap>();//鍥惧儚
	public static final List<Double> xysum = new ArrayList<Double>();//缁忓害绾害涔嬪拰鐢ㄤ簬鍒ゆ柇鐐瑰嚮宸插瓨鍦ㄧ殑鍐涙爣淇℃伅
}
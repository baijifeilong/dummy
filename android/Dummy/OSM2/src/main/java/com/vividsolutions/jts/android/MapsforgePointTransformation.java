package com.vividsolutions.jts.android;

import org.andnav.osm.util.GeoPoint;
import org.andnav.osm.views.OpenStreetMapView.OpenStreetMapViewProjection;

import android.graphics.Point;
import android.graphics.PointF;

import com.vividsolutions.jts.android.PointTransformation;
import com.vividsolutions.jts.geom.Coordinate;

/**
 * Transformation that handles mapsforge transforms.
 * @author Andrea Antonello (www.hydrologis.com)
 */
public class MapsforgePointTransformation implements PointTransformation {
    private byte drawZoom;
    private OpenStreetMapViewProjection  projection;
    private final Point tmpPoint = new Point();
    private Point drawPosition;

    public MapsforgePointTransformation( OpenStreetMapViewProjection  projection, Point drawPosition, byte drawZoom ) {
        this.projection = projection;
        this.drawPosition = drawPosition;
        this.drawZoom = drawZoom;
    }

    public void transform( Coordinate model, PointF view ) {
        projection.toPixels(GeoPoint.from2DoubleString(model.y+"", model.x+"") , tmpPoint);
        view.set(tmpPoint.x - drawPosition.x, tmpPoint.y - drawPosition.y);
    }
}
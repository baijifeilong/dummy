package bj.study.oschinastudy;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;



import com.nineoldandroids.animation.AnimatorSet;
import com.nineoldandroids.animation.ArgbEvaluator;
import com.nineoldandroids.animation.ObjectAnimator;
import com.nineoldandroids.animation.ValueAnimator;
import static com.nineoldandroids.view.ViewPropertyAnimator.animate;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @Bind(R.id.layout_main)
    DrawerLayout mainLayout;

    @Bind(R.id.menu_main)
    ListView mainMenu;

    @Bind(R.id.view_test)
    View testView;

    private String mTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        mainMenu.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_checked, new String[]{"apple", "banana", "orange"}) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                TextView view = (TextView) super.getView(position, convertView, parent);
                view.setTextColor(Color.RED);
                return view;
            }
        });
        mainMenu.setChoiceMode(ListView.CHOICE_MODE_SINGLE);

        mainLayout.openDrawer(Gravity.LEFT);
        getSupportActionBar().setIcon(R.drawable.blossom);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayUseLogoEnabled(true);
        getSupportActionBar().setTitle(mTitle);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.homeAsUp:
                mainLayout.openDrawer(Gravity.LEFT);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.view_test)
    protected void onClick(View v) {
        v.animate().setDuration(3000).scaleXBy(0.1f).setDuration(2000).start();
    }
}

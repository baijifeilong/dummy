package bj.study.oschinastudy;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;

import com.orhanobut.logger.Logger;
import com.taobao.android.dexposed.DexposedBridge;
import com.taobao.android.dexposed.XC_MethodHook;
import com.taobao.android.dexposed.XposedHelpers;

import timber.log.Timber;

/**
 * Created by Administrator on 2015/8/19.
 */
public class App extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        Timber.plant(new Timber.DebugTree());
        Logger.init("baiji");
        Logger.d("abc");
        Logger.v("avc");
        Logger.e("avc");
        Logger.wtf("fas");

        if (DexposedBridge.canDexposed(this)) {

            DexposedBridge.findAndHookMethod(Activity.class, "onCreate", Bundle.class, new XC_MethodHook() {
                @Override
                protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
                    Timber.d("xposed before hooked");
                    Activity activity = (Activity) param.thisObject;
                    XposedHelpers.setObjectField(activity, "mTitle", "Dexposed");
                }

                @Override
                protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                    Timber.d("xposed after hooked");
                }
            });
        } else {
            Timber.d("xposed cannot dexposed");
        }
    }
}

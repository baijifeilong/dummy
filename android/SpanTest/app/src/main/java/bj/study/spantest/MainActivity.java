package bj.study.spantest;

import android.graphics.Color;
import android.graphics.Paint;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.BackgroundColorSpan;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);

        LinearLayout linearLayout = new LinearLayout(this);
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        linearLayout.setGravity(Gravity.CENTER_VERTICAL);
        linearLayout.setPadding(10, 0, 10, 0);
        Button button = new Button(this);
        button.setText(Html.fromHtml("<i>请我</i><a>四个</a><b>Bold</b>"));
        button.getPaint().setFlags(Paint.UNDERLINE_TEXT_FLAG);

        TextView textView = new TextView(this);
        textView.setText(Html.fromHtml("<i>请我</i><a>四个</a><b>Bold</b>"));
        textView.getPaint().setFlags(Paint.UNDERLINE_TEXT_FLAG);
        textView.setMinimumHeight(70);
        textView.setBackgroundColor(Color.CYAN);
        textView.setGravity(Gravity.CENTER);

        TextView txtSpan = new TextView(this);
        SpannableString spannableString = new SpannableString("红橙黄绿蓝靛紫");
        spannableString.setSpan(new BackgroundColorSpan(Color.YELLOW), 0, 3, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        txtSpan.setText(spannableString);

        linearLayout.addView(button);
        linearLayout.addView(textView, 0);
        linearLayout.addView(txtSpan);
        setContentView(linearLayout);
    }
}

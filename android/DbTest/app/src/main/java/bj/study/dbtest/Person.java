package bj.study.dbtest;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

/**
 * Created by Administrator on 2015/8/14.
 */
public class Person implements Serializable, Parcelable{

    public int _id;
    public String name;
    public short age;
    public String info;

    public Person(String name, short age) {
        this.name = name;
        this.age = age;
    }

    public Person(String name, short age, String info) {
        this.name = name;
        this.age = age;
        this.info = info;
    }

    protected Person(Parcel in) {
        _id = in.readInt();
        name = in.readString();
        info = in.readString();
    }

    public static final Creator<Person> CREATOR = new Creator<Person>() {
        @Override
        public Person createFromParcel(Parcel in) {
            return new Person(in);
        }

        @Override
        public Person[] newArray(int size) {
            return new Person[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(_id);
        dest.writeString(name);
        dest.writeString(info);
    }
}

package bj.study.dbtest;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.CursorWrapper;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

import java.util.LinkedList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private DBManager dbManager;
    private ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


    }

    @Override
    protected void onDestroy() {
        dbManager.closeDB();
        super.onDestroy();
    }

    private void testDB(){
        SQLiteDatabase db = openOrCreateDatabase("test.db", MODE_PRIVATE, null);
        db.execSQL("DROP TABLE IF EXISTS person");
        db.execSQL("CREATE TABLE person(_id INTEGER PRIMARY KEY AUTOINCREMENT, name VARCHAR, age SMALLINT)");
        Person person = new Person("Alice", (short) 16);
        db.execSQL("INSERT INTO person VALUES(null, ?, ?)", new Object[]{person.name, person.age});

        person.name = "David";
        person.age = 33;
        ContentValues contentValues = new ContentValues();
        contentValues.put("name", person.name);
        contentValues.put("age", person.age);
        db.insert("person", null, contentValues);

        ContentValues cv = new ContentValues();
        cv.put("age", 35);
        db.update("person", cv, "name = ?", new String[]{"David"});

        Cursor cursor = db.rawQuery("SELECT * FROM person WHERE age > ?", new String[]{"2"});
        while (cursor.moveToNext()) {
            int _id = cursor.getInt(cursor.getColumnIndex("_id"));
            String name = cursor.getString(cursor.getColumnIndex("name"));
            short age = cursor.getShort(cursor.getColumnIndex("age"));
            Log.d("---", String.format("_id+name+age(%d, %s, %d)", _id, name, age));
        }
        cursor.close();
        db.close();
    }

    private void testDbHelper() {
        listView = (ListView)findViewById(R.id.listView);
        dbManager = new DBManager(this);

        List<Person> persons = new LinkedList<>();
        persons.add(new Person("Ella", (short)22, "lively girl"));
        persons.add(new Person("Jenny", (short) 22, "beautiful girl"));
        persons.add(new Person("Jessica", (short) 23, "sexy girl"));
        persons.add(new Person("Kelly", (short) 23, "hot baby"));
        persons.add(new Person("Jane", (short) 25, "a pretty woman"));
        dbManager.add(persons);

        Cursor cursor = dbManager.queryCursor();
        startManagingCursor(cursor);
        CursorWrapper cursorWrapper = new CursorWrapper(cursor) {
            @Override
            public String getString(int columnIndex) {
                if (getColumnName(columnIndex).equals("info")) {
                    return getString(getColumnIndex("age")) + " years old " + super.getString(columnIndex);
                }
                return super.getString(columnIndex);
            }
        };

        SimpleCursorAdapter simpleCursorAdapter = new SimpleCursorAdapter(this,
                android.R.layout.simple_list_item_2, cursorWrapper, new String[]{"name", "info"},
                new int[]{android.R.id.text1, android.R.id.text2});
        listView.setAdapter(simpleCursorAdapter);
    }
}

package bj.study.dbtest;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2015/8/14.
 */
public class DBManager {

    private DbHelper helper;
    private SQLiteDatabase db;

    public DBManager(Context context) {
        helper = new DbHelper(context);
        db = helper.getWritableDatabase();
    }

    public void add(List<Person> persons) {
        db.beginTransaction();
        try {
            for (Person person : persons) {
                db.execSQL("INSERT INTO person VALUES (null, ?, ?, ?)", new Object[]{person.name, person.age, person.info});
            }
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
    }

    public Cursor queryCursor() {
        return db.query("person", null, null, null, null, null, null);
    }

    public List<Person> query() {
        Cursor cursor = queryCursor();
        List<Person> persons = new ArrayList<>();
        while (cursor.moveToNext()) {
            Person person = new Person(cursor.getString(cursor.getColumnIndex("name")),
                    cursor.getShort(cursor.getColumnIndex("age")),
                    cursor.getString(cursor.getColumnIndex("info")));
            persons.add(person);
        }
        return persons;
    }

    public void closeDB() {
        db.close();
    }
}

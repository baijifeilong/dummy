import org.dom4j.Document
import org.dom4j.Element
import org.dom4j.io.SAXReader

File inputXml = new File('../asset/temp.xml')
SAXReader saxReader = new SAXReader();

Document document = saxReader.read(inputXml)
Element messages = document.getRootElement();
for (Iterator i = messages.elementIterator(); i.hasNext();) {
    Element message = (Element)i.next();
    for (Iterator j = message.elementIterator(); j.hasNext();) {
        Element node = (Element)j.next();
        println '+++++ ' + node.getName() + ": " + node.getText()
    }
    println '---------------'
}
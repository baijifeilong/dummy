import java.lang.Override;
import java.lang.String;
import java.lang.System;

public class Base {

    public static class InnerClass {
        public void hello() {
            System.out.println("Hello");
        }
    }

    public static void main(String args[]) {
        (new InnerClass() {
            @Override
            public void hello() {
                System.out.println("Inner hello");
            }
        }).hello();
    }
}
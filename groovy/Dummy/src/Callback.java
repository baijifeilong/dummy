import locaser.lscp.imp.LscpResponse;

public interface Callback {

	public void onSuccess(LscpResponse response);

	public void onFailed(LscpResponse response);

	public void onException(Exception ex);
}
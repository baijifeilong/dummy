import locaser.lscp.LscpMessageCodeFactory;
import locaser.lscp.LscpMessageDecoder;
import locaser.lscp.LscpMessageEncoder;
import locaser.lscp.imp.LscpRequest;
import locaser.lscp.imp.LscpResponse;
import org.apache.mina.core.service.IoConnector;
import org.apache.mina.core.service.IoHandlerAdapter;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolCodecFilter;
import org.apache.mina.transport.socket.nio.NioSocketConnector;

import java.net.InetSocketAddress;
import java.nio.charset.Charset;
import java.util.Map;

/**
 * Created by Administrator on 2015/8/31.
 */
public class Conn {

    public static void send(int functionId, Map map, final Callback callback) {
        IoConnector connector = new NioSocketConnector();
        connector.setConnectTimeoutMillis(10000);
        connector.getFilterChain().addLast("codec",
                new ProtocolCodecFilter(new LscpMessageCodeFactory(new LscpMessageDecoder(Charset.forName("UTF-8")),
                        new LscpMessageEncoder(Charset.forName("UTF-8")))));
        connector.setHandler(new IoHandlerAdapter() {

            @Override
            public void exceptionCaught(IoSession session, Throwable cause) throws Exception {
                System.out.println("///Exception");
                callback.onException(new Exception("Server exception"));
            }

            @Override
            public void messageReceived(IoSession session, Object message) throws Exception {
                if (message instanceof LscpResponse) {
                    System.out.println(String.format("\n[接收] %d-%d-%s", ((LscpResponse) message).getFunctionid(),
                            ((LscpResponse) message).getState(), ((LscpResponse) message).getContentXML()));
                    LscpResponse response = (LscpResponse) message;
                    if (response.getState() == LscpResponse.STATE_OK || response.getState() == 101) {
                        System.out.println("[状态] Success");
                        callback.onSuccess(response);
                    } else {
                        System.out.println("[状态] Failed");
                        callback.onFailed(response);
                    }
                }
            }

            @Override
            public void messageSent(IoSession session, Object message) throws Exception {
                System.out.println(String.format("\n[发送] %d-%s", ((LscpRequest)message).getFunctionid(),
                        ((LscpRequest)message).getContent()));
            }
        });

        IoSession session;
        try {
            session = connector.connect(new InetSocketAddress(Config.hostname, Config.port)).awaitUninterruptibly()
                    .getSession();
        } catch (RuntimeException ex) {
            ex.printStackTrace();
            return;
        }

        LscpRequest request = new LscpRequest();
        request.setFunctionid(functionId);
        request.setContent(map);
        request.setAlive(1);
        session.write(request).awaitUninterruptibly();
        session.getCloseFuture().awaitUninterruptibly(5000);
        connector.dispose();
    }
}
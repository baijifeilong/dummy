/**
 * Created by Administrator on 2015/8/27.
 */
class User {

    private static User user;

    private User() {}

    public static User get() {
        return user == null ? new User() : user
    }

    private String id;
    private String sessionId;
    private String nickname;
}

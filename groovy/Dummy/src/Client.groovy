import com.google.common.collect.ImmutableMap
import com.google.common.collect.Maps
import locaser.lscp.LscpMessageCodeFactory;
import locaser.lscp.LscpMessageDecoder;
import locaser.lscp.LscpMessageEncoder
import locaser.lscp.imp.LscpRequest
import locaser.lscp.imp.LscpResponse;
import org.apache.mina.core.service.IoConnector;
import org.apache.mina.core.service.IoHandler;
import org.apache.mina.core.session.IdleStatus;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolCodecFilter;
import org.apache.mina.transport.socket.nio.NioSocketConnector;

import java.nio.charset.Charset;

/**
 * Created by Administrator on 2015/8/27.
 */
public class Client implements IoHandler {

    private static Client client;
    IoConnector connector;
    IoSession session;

    public static Client get() {
        return client = client == null? new Client() : client;
    }

    private Client() {
        connector = new NioSocketConnector();
        connector.setConnectTimeoutMillis(9999);
        connector.getFilterChain().addLast("codec", new ProtocolCodecFilter(
                new LscpMessageCodeFactory(new LscpMessageDecoder(Charset.forName("UTF-8")),
                        new LscpMessageEncoder(Charset.forName("UTF-8")))));
        connector.setHandler(this);
    }

    public boolean connect() {
        try {
            session = connector.connect(new InetSocketAddress(Config.hostname, Config.port)).awaitUninterruptibly().getSession()
            println session
        } catch (RuntimeException ignored) {
            return false;
        }
        return true;
    }

    public boolean login() {
        LscpRequest lscpRequest = new LscpRequest();
        lscpRequest.setVersion(1);
        lscpRequest.setAlive(1);
        lscpRequest.setFunctionid(2000);
        lscpRequest.setContent(ImmutableMap.of("loginname", "xj4", "password", "111111", "type", "3"))

        session.write(lscpRequest).awaitUninterruptibly()
    }

    public void getSms() {
        LscpRequest lscpRequest = new LscpRequest();
        lscpRequest.setVersion(1);
        lscpRequest.setAlive(1);
        lscpRequest.setFunctionid(4100);
        lscpRequest.setContent(ImmutableMap.of("userid", "117", 'msgtype', '2'))

        session.write(lscpRequest).awaitUninterruptibly()
    }

    @Override
    public void sessionCreated(IoSession ioSession) throws Exception {

    }

    @Override
    public void sessionOpened(IoSession ioSession) throws Exception {

    }

    @Override
    public void sessionClosed(IoSession ioSession) throws Exception {

    }

    @Override
    public void sessionIdle(IoSession ioSession, IdleStatus idleStatus) throws Exception {

    }

    @Override
    public void exceptionCaught(IoSession ioSession, Throwable throwable) throws Exception {

    }

    @Override
    public void messageReceived(IoSession ioSession, Object o) throws Exception {
        LscpResponse lscpResponse = (LscpResponse)o;
        println("\n\n" + lscpResponse.functionid + '---' + lscpResponse.state + '---' + lscpResponse.contentXML)
    }

    @Override
    public void messageSent(IoSession ioSession, Object o) throws Exception {

    }

    @Override
    public void inputClosed(IoSession ioSession) throws Exception {

    }
}

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;


/**
 * 解析登录响应的XML
 * 
 * @class UserXmlParser
 * @author BaiJiFeiLong@gmail.com
 * @date 2015年8月6日 上午11:30:27
 */
public class UserXmlParser extends DefaultHandler {

	private String node;

	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
		node = qName;
	}

//	@Override
//	public void characters(char[] ch, int start, int length) throws SAXException {
//		String value = new String(ch, start, length);
//		if (node.equals(ProtocolConstant.TAG_NICKNAME)) {
//			User.get().setNickname(value);
//		} else if (node.equals(ProtocolConstant.TAG_LOGIN_USER_ID)) {
//			User.get().setId(Integer.parseInt(value));
//		} else if (node.equals(ProtocolConstant.TAG_SESSION_ID)) {
//			User.get().setSessionId(Integer.parseInt(value));
//		}
//	}
}